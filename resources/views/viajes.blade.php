@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="card bg-dark text-black">
            <img src="{{url('/Viajeros.jpg')}}" class="card-img" style="height: 300px">
            <div class="card-img-overlay">
                <h5 class="card-title"></h5>
                <h3 class="card-text" style="text-align:center;">"Viaja por Nariño, conoce lo nuestro"</h3>
                <h1 class="card-text" align="center"><ion-icon name="heart-outline"></ion-icon></h1>
            </div>
        </div>   
    
        <div class="row justify-content-center mt-5 pt-5" style="background-color: #4a4a4a6c ">   

            @foreach ($listaSitios as $key => $sitio)
                <div  class="col-md-3 card" style=" margin: 10px; background-color: rgba(248, 236, 236, 0.66)" >                    
                    <a href="{{ url('descripcion/' . $sitio->id ) }}" class="btn btn-outline-info">
                        <img src="{{$sitio->galeriaS }}" class="card-img-top img-thumbnail" style="height: 150px; width: 150px">
                        <input type="hidden" value="{{$sitio->id}}" />
                        <div class="card-body" align="center">
                            <h5>{{$sitio->nombre}}</h5>
                        </div>
                        <ion-icon name="arrow-forward-circle-outline" size="large"></ion-icon>
                    </a>
                </div>
            @endforeach

        </div>   
    </div> 
@stop