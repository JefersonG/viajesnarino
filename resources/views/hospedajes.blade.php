@extends('layouts.master')
@section('content')
    @if(Auth::user()->rol=="ADMIN")
                
        <div class="container" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12)" >
            <div class="row justify-content-center mt-5 pt-5" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12)">
                <div class="col-md-1"></div>
                
                <div class="col-md-10">
                    <p class="tittle" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12); color:lightgrey" align="center"> Aministracion de hospedajes </p>
                    <button class="btn btn-success btn-lg btn-block" id="btn-abrir-popup">
                        <ion-icon name="add-circle-outline"></ion-icon>
                        Agregar un hospedaje
                    </button>
                </div>
                
                <div class="col-md-1"></div>
            </div>
        </div>        

        <div class="container" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12); height:500px; overflow: scroll" >

            @foreach ($listahospedajes as $key => $hospedaje )
                <form method="POST" action="" id="miFormulario" >
                    {{method_field('PUT')}}
                    {{ csrf_field()}}
                    <div class="row justify-content-center mt-5 pt-5" style="background-color: rgba(248, 236, 236, 0.348)">
                        <div class="col-md-1"></div>

                        <div class="col-md-3">           
                            {{-- TODO: Imagen de hospedaje --}}  
                            <img src="{{ url($hospedaje->galeriaS)}}" style="height: 150px; width: 180px"  class="card-img-top img-thumbnail" />
                            <input type="hidden" name="id" id ="id" value="{{$hospedaje->id}}" />
                        </div> 

                        <div class="col-md-6">
                            <h6> <ion-icon name="home-outline"></ion-icon> Nombre: {{$hospedaje->nombre}}</h6>
                            <h6> <ion-icon name="bed-outline"></ion-icon> N° habitaciones: {{$hospedaje->nHabitaciones}}</h6>
                            <h6> <ion-icon name="location-outline"></ion-icon> Dirección: {{$hospedaje->direccion}}</h6>
                            <h6> <ion-icon name="cash-outline"></ion-icon> Precio: {{number_format($hospedaje->precio)}}</h6>
                            <h6> <ion-icon name="call-outline"></ion-icon> Telefono: {{$hospedaje->telefono}}</h6>
                            <h6> <ion-icon name="map-outline"></ion-icon> Zona: {{$hospedaje->zona}}</h6>
                            
                        </div>

                        <div class="col-md-1">
                            <button type="submit" class="btn btn-danger">
                                <ion-icon name="trash-outline"></ion-icon>
                                Eliminar
                            </button>
                            <a type="button" class="btn btn-warning" href="{{url('/edithospedaje/'.$hospedaje->id)}}">
                                <ion-icon name="create-outline"></ion-icon>
                                Editar hsopedaje
                            </a>
                        </div>

                        <div class="col-md-1"></div>
                        
                    </div>
                </form>
            @endforeach
        </div>

        <div class="contenedor">    
            <div class= "overlay" id="overlay">
                <div class="popup" id="popup"> 
                    <a href="" id="btn-cerrar-popup1" name="btn-cerrar-popup" class="btn-cerrar-popup">
                        <ion-icon name="close-outline"></ion-icon>
                    </a>
                    <h3> Digite los campos </h3>
                    <form action="" method="POST" enctype="multipart/form-data">
                        {{ csrf_field()}}
                        <div class="contenedor-inputs">
                            <input id ="nombre" name="nombre" type="text" placeholder="Nombre">
                            <input id ="nHabitaciones" name="nHabitaciones" type="number" placeholder="Numero de habitaciones">
                            <strong>Seleccionar Imagen</strong></br>
                            <input type="file" name="galeriaS" id="galeriaS"></br>
                            <input id ="precio" name="precio" type="number" placeholder="Precio">
                            <input id ="telefono" name="telefono" type="number" placeholder="Teléfono">
                            <input id ="direccion" name="direccion" type="text" placeholder="Dirección">
                            <select id ="zona" name="zona">
                                <option value="zona1">Zona1</option>
                                <option value="zona2" selected>Zona2</option>
                                <option value="zona3">Zona3</option>
                            </select>
                            <input type="submit" class ="btn-submit" value="Crear Hospedaje">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endif
    <script>
        (function() {
          var form = document.getElementById('miFormulario');
          form.addEventListener('submit', function(event) {
            // si es false entonces que no haga el submit
            if (!confirm('Realmente desea eliminar?')) {
              event.preventDefault();
            }
          }, false);
        })();
    </script>
@stop