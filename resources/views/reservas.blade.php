@extends('layouts.master')
@section('content')
    <div class="container" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12); height:5px;" align="center">
        <ion-icon name="clipboard-outline"></ion-icon>
        Mis Reservas
    </div>

    <div class="container" style="background-color:#4a4a4a6c; height:480px; overflow: scroll" >
        
        {{$img="",$nombre="",$p1=0,$p2=0,$p3=0,$tipo="",$placa=""}}

        @foreach ($listareservas as $key => $reserva )
            <div class="row justify-content-center mt-5 pt-5" style="background-color: rgba(248, 236, 236, 0.348)">            
                @foreach ($listasitios as $key1 => $sitio) 
                    @if ($sitio->id == $reserva->id_sitio)
                        <input type="hidden" value= "{{$img= $sitio->galeriaS}}" /> 
                        <input type="hidden" value= "{{$nombre = $sitio->nombre}}"/>
                        <input type="hidden" value= "{{$p1 = $sitio->precio}}"/>
                    @endif          
                @endforeach

                @foreach ($listatransportes as $key3 => $tra) 
                    @if ($tra->id == $reserva->id_transporte)
                        <input type="hidden" value= "{{$tipo = $tra->tipo}}">
                        <input type="hidden" value= "{{$placa = $tra->placas}}">
                        <input type="hidden" value= "{{$p2 = $tra->precio}}">
                    @endif
                @endforeach

                @foreach ($listahospedajes as $key2 => $hosp) 
                    @if ($hosp->id == $reserva->id_hospedaje)
                        <input type="hidden" value= "{{$p3 = $hosp->precio}}"/>
                    @endif
                @endforeach
                
                <div class="col-md-2">
                    <img src="{{ url($img)}}" style="height: 150px; width: 150px" class="card-img-top img-thumbnail" />
                </div>
            
                <div class="col-md-3">
                    <h6> <ion-icon name="map-outline"></ion-icon> Sitio: {{$nombre}} </h6>
                    <h6> <ion-icon name="pricetags-outline"></ion-icon> Precio: {{number_format($p1+$p2+$p3)}} </h6>
                    
                </div>

                <div class="col-md-3">
                    <h6> <ion-icon name="bus-outline"></ion-icon> Transporte: {{$tipo}} </h6>
                    <h6> <ion-icon name="newspaper-outline"></ion-icon> N° Placa: {{$placa}} </h6>

                </div>

                <div class="col-md-3">
                    <h6> <ion-icon name="calendar-clear-outline"></ion-icon> Fecha salida: {{$reserva->fecha_ini}} </h6>
                    <h6> <ion-icon name="calendar-number-outline"></ion-icon> Fecha llegada: {{$reserva->fecha_fin}}</h6>                                 
                </div>

                <div class="col-md-1"> </div>

            </div>
        @endforeach
        
    </div>
    <div class="container" style="background-color:#4a4a4a6c; color:lightgrey" align="center">
        <a type="button" class="btn btn-info" href="{{url('/')}}">
            <ion-icon name="home-outline"></ion-icon>
            Volver
        </a>   
    </div>
@stop