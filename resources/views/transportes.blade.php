@extends('layouts.master')
@section('content')
    @if(Auth::user()->rol=="ADMIN")
        
        <div class="container" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12)" >
            <div class="row justify-content-center mt-5 pt-5" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12)">
                <div class="col-md-1"></div>
                
                <div class="col-md-10">
                    <p class="tittle" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12); color:lightgrey" align="center"> Aministracion de transportes </p>
                    <button class="btn btn-success btn-lg btn-block" id="btn-abrir-popup">
                        <ion-icon name="add-circle-outline"></ion-icon>
                        Agregar un transporte
                    </button>
                </div>
                
                <div class="col-md-1"></div>
            </div>
        </div>

        <div class="container" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12); height:500px; overflow: scroll" >
            @foreach ($listatransportes as $key => $transporte )
                <form method="POST" action="" id="miFormulario">
                    {{method_field('PUT')}}
                    {{ csrf_field()}}
                    <div class="row justify-content-center mt-5 pt-5" style="background-color: rgba(248, 236, 236, 0.348)">
                        <div class="col-md-1"></div>

                        <div class="col-md-6" >
                            <input type="hidden" name="id" id ="id" value="{{$transporte->id}}" />
                            <h6> <ion-icon name="bus-outline"></ion-icon> Tipo: {{$transporte->tipo}}</h6>
                            <h6> <ion-icon name="clipboard-outline"></ion-icon> Placas: {{$transporte->placas}}</h6>
                        </div>

                        <div class="col-md-4">
                            <button type="submit" class="btn btn-danger" >
                                <ion-icon name="trash-outline"></ion-icon>
                                Eliminar
                            </button>

                            <button type="submit" class="btn btn-info">
                                <ion-icon name="create-outline"></ion-icon>
                                Modificar
                            </button>  
                        </div>

                        <div class="col-md-1"></div>
                    </div>
                </form>
            @endforeach
        </div>

        <div class="contenedor">    
            <div class= "overlay" id="overlay">
                <div class="popup" id="popup"> 
                    <a href="" id="btn-cerrar-popup" name="btn-cerrar-popup" class="btn-cerrar-popup">
                        <ion-icon name="close-outline"></ion-icon>
                    </a>
                    <h3> Digite los campos </h3>
                    <form action="" method="POST">
                        {{ csrf_field()}}
                        <div class="contenedor-inputs">
                            <input id ="tipo" name="tipo" type="text" placeholder="Tipo de transporte">
                            <input id ="placas" name="placas" type="text" placeholder="Placas del vehiculo">
                            <input id ="precio" name="precio" type="number" placeholder="Precio">
                            <input type="submit" class ="btn-submit" value="Crear Hospedaje">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    @endif
    <script>
        (function() {
          var form = document.getElementById('miFormulario');
          form.addEventListener('submit', function(event) {
            // si es false entonces que no haga el submit
            if (!confirm('Realmente desea eliminar?')) {
              event.preventDefault();
            }
          }, false);
        })();
    </script>
@stop