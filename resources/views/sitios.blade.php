@extends('layouts.master')
@section('content')
    @if(Auth::user()->rol=="ADMIN")
        
    <div class="container" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12)" >
        <div class="row justify-content-center mt-5 pt-5" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12)">
            <div class="col-md-1"></div>
            
            <div class="col-md-10" >
                <p class="tittle" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12); color:lightgrey" align="center"> Aministracion de sitios </p>
                <button class="btn btn-success btn-lg btn-block" id="btn-abrir-popup">
                    <ion-icon name="add-circle-outline"></ion-icon>
                    Agregar un sitio
                </button>
            </div>
            
            <div class="col-md-1"></div>
        </div>
    </div>

    <div class="container" style="background-color:#4a4a4a6c; color:rgb(14, 12, 12); height:500px; overflow: scroll" >

        @foreach ($listaSitios as $key => $sitio )
            <form method='POST' action="" id="miFormulario">
                {{method_field('PUT')}}
                {{ csrf_field()}}
                <div class="row justify-content-center mt-5 pt-5" style="background-color: rgba(248, 236, 236, 0.348)">
                    <div class="col-md-1"></div>

                    <div class="col-md-3" align="center">           
                        {{-- TODO: Imagen de la película --}}  
                        <img src="{{ url($sitio->galeriaS)}}" style="height: 150px; width: 150px" class="card-img-top img-thumbnail" />
                        <input type="hidden" name="id" id ="id" value="{{$sitio->id}}" />
                    </div> 
                    
                    <div class="col-md-5">
                        <h5> <ion-icon name="bus-outline"></ion-icon> Nombre: {{$sitio->nombre}}</h5>
                        <h5> <ion-icon name="pencil-outline"></ion-icon> Descripción: {{$sitio->descripcion}}</h5>
                        <h5> <ion-icon name="map-outline"></ion-icon> Zona: {{$sitio->zona}}</h5>
                        <h5> <ion-icon name="cash-outline"></ion-icon> Precio: {{number_format($sitio->precio)}}</h5>
                    </div>

                    <div class="col-md-2">
                        <button type="submit" class="btn btn-danger">
                            <ion-icon name="trash-outline"></ion-icon>
                            Eliminar
                        </button>
                        <button type="submit" class="btn btn-info">
                            <ion-icon name="create-outline"></ion-icon>
                            Modificar
                        </button>     
                    </div>
                    
                    <div class="col-md-1"></div>
                </div>
            </form>
        @endforeach
    </div>
    
    <div class="contenedor">    
        <div class= "overlay" id="overlay">
            <div class="popup" id="popup"> 
                <a href="" id="btn-cerrar-popup" name="btn-cerrar-popup" class="btn-cerrar-popup">
                    <ion-icon name="close-outline"></ion-icon>
                </a>
                <h3> Digite los campos </h3>
                <form action="" method="POST" enctype="multipart/form-data">
                    {{ csrf_field()}}
                    <div class="contenedor-inputs">
                        <input id ="nombre" name="nombre" type="text" placeholder="Nombre">
                        <input id ="descripcion" name="descripcion" type="text" placeholder="Descripción">
                        <input id ="precio" name="precio" type="number" placeholder="Precio">
                        <strong>Seleccionar Imagen</strong>
                        <input type="file" name="galeriaS" id="galeriaS">

                        <select id ="zona" name="zona">
                            <option value="zona1">Zona1</option>
                            <option value="zona2" selected>Zona2</option>
                            <option value="zona3">Zona3</option>
                        </select>
                        <input type="submit" class ="btn-submit" value="Crear Sitio">
                    </div>
                </form>

            </div>
        </div>
    </div>
    
    @endif
    <script>
        (function() {
          var form = document.getElementById('miFormulario');
          form.addEventListener('submit', function(event) {
            // si es false entonces que no haga el submit
            if (!confirm('Realmente desea eliminar?')) {
              event.preventDefault();
            }
          }, false);
        })();
    </script>
@stop