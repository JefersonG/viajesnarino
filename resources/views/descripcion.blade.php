@extends('layouts.master')
@section('content')
<div class="container" style="background-color: #4a4a4a6c ">
    <form method="POST" action="">
        {{ csrf_field()}}
        <div class="row justify-content-center mt-5 pt-5">
            @if (Auth::check()) <input type="hidden" id="id_usuario" name="id_usuario" value="{{Auth::user()->id}}" /> @endif

            <div class="col-md-1" > </div>

            <div class="col-md-4" >
                <img src="{{ url($sitio->galeriaS)}}" class="card-img-top img-thumbnail" style="height: 200px; width: 250px; align-items: center;" />
                <h6> <ion-icon name="caret-down-outline"></ion-icon> </h6>
                <h6> <ion-icon name="map-outline"></ion-icon> {{$sitio->nombre}}</h6>
                <h6> <ion-icon name="pricetags-outline"></ion-icon> [$COL={{number_format($sitio->precio)}}] </h6>
                <h6> <ion-icon name="clipboard-outline"></ion-icon> {{$sitio->descripcion}}</h6>
                <h6> 
                    <ion-icon name="calendar-outline"></ion-icon>
                    Fecha partida:<input type="date" name ="fecha_ini" id="fecha_ini" style="background-color: #4a4a4a6c ">
                </h6>
                <h6>
                    <ion-icon name="calendar-outline"></ion-icon>
                    Fecha llegada:<input type="date" name ="fecha_fin" id="fecha_fin" style="background-color: #4a4a4a6c ">
                </h6>
                <input type="hidden" id="id_sitio" name="id_sitio" value="{{$sitio->id}}" />
            </div>

            <div class="col-md-7" >
                <div class="container" style="height:200px; overflow: scroll">
                    @foreach ($listahospedajes as $key => $hospedaje )
                        <div class="row justify-content-center mt-5 pt-5" style="background-color: rgba(248, 236, 236, 0.348)">
                            <div class="col-md-3">            
                                <img src="{{ url($hospedaje->galeriaS)}}" class="card-img-top img-thumbnail" style="height: 90px; width: 90px" />
                            </div> 

                            <div class="col-md-8">
                                <h6> <ion-icon name="home-outline"></ion-icon> {{$hospedaje->nombre}}</h6>
                                <h6> <ion-icon name="location-outline"></ion-icon> {{$hospedaje->direccion}}</h6>
                                <h6> <ion-icon name="call-outline"></ion-icon> {{$hospedaje->telefono}}</h6>
                                <h6> <ion-icon name="pricetag-outline"></ion-icon> {{number_format($hospedaje->precio)}}</h6>
                            </div>
                            <div class="col-md-1">
                                <input type="checkbox" id="id_hospedaje" name="id_hospedaje" value="{{$hospedaje->id}}">
                            </div>
                        </div>
                    @endforeach   
                </div>

                <div class="container" style="height:170px; overflow: scroll">
                    @foreach ($listatransportes as $key => $transporte )
                        <div class="row justify-content-center mt-5 pt-5" style="background-color: rgba(248, 236, 236, 0.348)">
                            <div class="col-md-11">
                                <h6> <ion-icon name="car-outline"></ion-icon> {{$transporte->tipo}}</h6>
                                <h6> <ion-icon name="wallet-outline"></ion-icon> {{$transporte->placas}}</h6>
                                <h6> <ion-icon name="pricetag-outline"></ion-icon> {{number_format($transporte->precio)}}</h6>
                            </div>

                            <div class="col-md-1">
                                <input type="checkbox" id="id_transporte" name="id_transporte" value="{{$transporte->id}}" style="background-color: #4a4a4a6c ">
                            </div>
                        </div>
                    @endforeach 
                </div>

                <div div class="container" style="height:75px">
                    <div class="row">
                        @if(Auth::check() and Auth::user()->rol=="CLIENT")
                            <button type="submit" class="btn btn-success">Reservar</button>
                            
                        @else
                            @if (Auth::check() and Auth::user()->rol!="ADMIN")
                            
                            @else
                                <div class="col-md-4"></div>
                                <div class="col-md-2"> <a href="{{ url('login/') }}" class="btn btn-info">Logearse</a> </div>
                                <div class="col-md-2"> <a href="{{ url('registro/') }}" class="btn btn-info">Registrarse</a> </div>
                                <div class="col-md-4"></div>
                            @endif
                        @endif
                        
                    </div> 
                </div>
            </div>

        
        </div>

        
    </form>
</div>
@stop