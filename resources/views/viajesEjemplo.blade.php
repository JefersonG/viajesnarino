@extends('layouts.master')
 
@section('content')
<div id="fh5co-wrapper">
    <div id="fh5co-page">
    <div id="fh5co-header">
        <header id="fh5co-header-section">
            <div class="container">
                <div class="nav-header">
                    <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                    <h1 id="fh5co-logo"><a href="index.html">Listing</a></h1>
                    <!-- START #fh5co-menu-wrap -->
                    
                </div>
            </div>
        </header>
        
    </div>
    
    
    <div class="fh5co-hero">
        <div class="fh5co-overlay"></div>
        <div class="fh5co-cover text-center" data-stellar-background-ratio="0.5" style="background-image: url(/assets/bootstrap/styloviajes/images/cover_bg_1.jpg);">
            <div class="desc animate-box">
                <h2>Viajemos por Nariño.</h2>
                <!-- <span>Lovely Crafted by <a href="http://frehtml5.co/" target="_blank" class="fh5co-site-name">FREEHTML5.co</a></span> -->
                <span><a class="btn btn-primary btn-lg" href="#">Inicia ya</a></span>
            </div>
        </div>

    </div>


    <div class="fh5co-listing">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 fh5co-item-wrap">
                    <a class="fh5co-listing-item">
                        <img src="{{url('/assets/bootstrap/styloviajes/images/img-1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co" class="img-responsive">
                        <div class="fh5co-listing-copy">
                            <h2>Santuario de Las Lajas</h2>
                            <span class="icon">
                                <i class="icon-chevron-right"></i>
                            </span>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 fh5co-item-wrap">
                    <a class="fh5co-listing-item">
                        <img src="{{url('/assets/bootstrap/styloviajes/images/img-2.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co" class="img-responsive">
                        <div class="fh5co-listing-copy">
                            <h2>Chachagüí</h2>
                            <span class="icon">
                                <i class="icon-chevron-right"></i>
                            </span>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 fh5co-item-wrap">
                    <a class="fh5co-listing-item">
                        <img src="{{url('/assets/bootstrap/styloviajes/images/img-3.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co" class="img-responsive">
                        <div class="fh5co-listing-copy">
                            <h2>Laguna de la Cocha</h2>
                            <span class="icon">
                                <i class="icon-chevron-right"></i>
                            </span>
                        </div>
                    </a>
                </div>
                <!-- END 3 row -->

                <div class="col-md-4 col-sm-4 fh5co-item-wrap">
                    <a class="fh5co-listing-item">
                        <img src="{{url('/assets/bootstrap/styloviajes/images/img-4.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co" class="img-responsive">
                        <div class="fh5co-listing-copy">
                            <h2>Amsterdam</h2>
                            <span class="icon">
                                <i class="icon-chevron-right"></i>
                            </span>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 fh5co-item-wrap">
                    <a class="fh5co-listing-item">
                        <img src="{{url('/assets/bootstrap/styloviajes/images/img-5.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co" class="img-responsive">
                        <div class="fh5co-listing-copy">
                            <h2>Australia</h2>
                            <span class="icon">
                                <i class="icon-chevron-right"></i>
                            </span>
                        </div>
                    </a>
                </div>
                <div class="col-md-4 col-sm-4 fh5co-item-wrap">
                    <a class="fh5co-listing-item">
                        <img src="{{url('/assets/bootstrap/styloviajes/images/img-6.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co" class="img-responsive">
                        <div class="fh5co-listing-copy">
                            <h2>Japan</h2>
                            <span class="icon">
                                <i class="icon-chevron-right"></i>
                            </span>
                        </div>
                    </a>
                </div>
                <!-- END 3 row -->


            </div>
        </div>
    </div>


    <div class="fh5co-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 fh5co-news">
                    <h3>News</h3>
                    <ul>
                        <li>
                            <a href="#">
                                <span class="fh5co-date">Sep. 10, 2016</span>
                                <h3>Newly done Bridge of London</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus reprehenderit!</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fh5co-date">Sep. 10, 2016</span>
                                <h3>Newly done Bridge of London</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus reprehenderit!</p>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="fh5co-date">Sep. 10, 2016</span>
                                <h3>Newly done Bridge of London</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus reprehenderit!</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 fh5co-testimonial">
                    <img src="{{url('/assets/bootstrap/styloviajes/images/cover_bg_1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co" class="img-responsive mb20">
                    <img src="{{url('/assets/bootstrap/styloviajes/images/cover_bg_1.jpg') }}" alt="Free HTML5 Bootstrap Template by FreeHTML5.co" class="img-responsive">
                </div>
            </div>
        </div>
    </div>
    
    <footer>
        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <p class="fh5co-social-icons">
                            <a href="#"><i class="icon-twitter2"></i></a>
                            <a href="#"><i class="icon-facebook2"></i></a>
                            <a href="#"><i class="icon-instagram"></i></a>
                            <a href="#"><i class="icon-dribbble2"></i></a>
                            <a href="#"><i class="icon-youtube"></i></a>
                        </p>
                        <p><a href="#">Contactanos</a> <br> <a href="http://freehtml5.co/" target="_blank"></a> <a href="https://unsplash.com/" target="_blank"></a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>


</div>
<!-- END fh5co-page -->

</div>
@stop