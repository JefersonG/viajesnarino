@extends('layouts.master')

@section('content')
 <div class="row" style="margin-top:40px">
        <div class="offset-md-3 col-md-6">
            <div class="card">
                <div class="card-header text-center">
                    Modificar hospedaje
                </div>
                <div class="card-body" style="padding:30px">

                    {{-- TODO: Abrir el formulario e indicar el método POST --}}
                    <form method="POST" enctype="multipart/form-data">
                    {{method_field('PUT')}}
                    
                    {{-- TODO: Protección contra CSRF --}}
                    {{ csrf_field() }}
                    
                    {{-- @foreach( $newhosp as $hosp ) --}}
                        <div class="contenedor-inputs">
                            <input id ="nombre" name="nombre" type="text" placeholder="Nombre" value="{{$hosp->nombre}}">
                            <input id ="nHabitaciones" name="nHabitaciones" type="number" placeholder="Numero de habitaciones" value="{{$hosp->nHabitaciones}}">
                            <strong>Seleccionar Imagen</strong></br>
                            <input type="file" name="galeriaS" id="galeriaS" value="{{$hosp->galeriaS}}"></br>
                            <input id ="precio" name="precio" type="number" placeholder="Precio" value="{{$hosp->precio}}">
                            <input id ="telefono" name="telefono" type="number" placeholder="Teléfono" value="{{$hosp->telefono}}">
                            <input id ="direccion" name="direccion" type="text" placeholder="Dirección" value="{{$hosp->direccion }}">
                            <select id ="zona" name="zona" value="{{$hosp->zona}}">
                                <option value="zona1">Zona1</option>
                                <option value="zona2" selected>Zona2</option>
                                <option value="zona3">Zona3</option>
                            </select>
                        </div>
                    {{-- @endforeach --}}
                    <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary" style="padding:8px 100px;margin-top:25px;">
                        Modificar hospedaje
                    </button>
                    </div>

                    {{-- TODO: Cerrar formulario --}}
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
