<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{url('/assets/bootstrap/css/bootstrap.min.css') }}">  
    <link rel="stylesheet" href="{{url('/assets/bootstrap/css/login.css') }}">
    <link rel="stylesheet" href="{{url('/assets/bootstrap/css/estilos.css') }}">


    <title>Viajes</title>
  </head>
  <body>
    @include('partials.navbarHome')

    <div class="container">
      @yield('content')
      <footer class=" footer-styl">
        <div class="container">
            <div class="row">
                <div class="col-md-4 ">
                    <h3>CONTACTOS</h3>
                    <p>
                        Correo: viajesnarino@gmail.com<br />
                        Celular: 3167020584<br />
                        San Juan de Pasto<br />
                    </p>
                </div>
                <div class="col-md-4">
                    <h3>Mis redes</h3>
                    <div class="sociales">
                            <a><ion-icon name="logo-facebook"></ion-icon></a>
                            <a><ion-icon name="logo-whatsapp"></ion-icon></a>
                            <a><ion-icon name="logo-instagram"></ion-icon></a>
                            <a><ion-icon name="logo-twitter"></ion-icon></a>
                    </div>
                </div>
                <div class="col-md-4 footer-col">
                    <h3>Viajemos por Nariño</h3>
                    <ion-icon name="airplane-outline"></ion-icon>
                </div>
            </div>
        </div>
      </footer>
  
    </div>
    
    
    <script src="{{ url('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ url('/assets/bootstrap/js/login.js') }}"></script>
    <script src="{{ url('/assets/bootstrap/js/popup.js') }}"></script>


    <script src="https://unpkg.com/ionicons@5.4.0/dist/ionicons.js"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

  </body>
</html>
