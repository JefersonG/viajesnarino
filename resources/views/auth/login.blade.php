@extends('layouts.master')

@section('content')
<div class="wrapper">
    <form class="login" method="POST" action="{{ route('login') }}">
        @csrf
        <p class="title">Iniciar Sesíon</p>

        <!-- campo email-->
        <input id="email" type="email" style="color:#ebe6e6" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Correo" autofocus>

        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <i class="fa fa-user"></i>

        <!-- campo contraseña -->
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña">

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <i class="fa fa-key"></i>

        <!-- Recuerdame -->
        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

        <label class="form-check-label" for="remember">
            {{ __('Recuerdame') }}
        </label>
        <i class="fa fa-key"><br></i>

        <!-- enlace de olvido su contraseña -->
        @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}">
                {{ __('¿olvidaste tu contraseña?') }}
            </a>
        @endif
        
        <!-- Boton enviar formulario -->
        <button type="submit" >
            <i class="spinner"></i>
            <span class="state">Ingresar</span>
        </button>

      
    </form>

</div>
@endsection
