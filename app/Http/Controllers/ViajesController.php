<?php

namespace App\Http\Controllers;

use App\Hospedaje;
use App\Reserva;
use App\Sitio;
use App\Transporte;
use Facade\FlareClient\Stacktrace\File;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ViajesController extends Controller
{
    //
    public function getIndex()
    {
        $sitios = DB::table('sitios')->get();
        return view('viajes',['listaSitios'=>$sitios]); 
    }
    public function getReservas()
    {
        $sitios = array();
        $hospedajes = array();
        $transportes = array();  
        $reserva = DB::table('reservas')->where('id_usuario',Auth::user()->id)->get();
        foreach ($reserva as $res)
        {
            array_push($sitios,Sitio::findOrFail($res->id_sitio));
            array_push($hospedajes,Hospedaje::findOrFail($res->id_hospedaje));
            array_push($transportes,Transporte::findOrFail($res->id_transporte));
        } 
        return view('reservas', ['listareservas'=>$reserva,'listasitios'=>$sitios,'listahospedajes'=>$hospedajes,'listatransportes'=>$transportes]);
    }
    public function getDescripcion($id)
    {
        $sitio = Sitio::findOrFail($id);
        $hospedajes = DB::table('hospedajes')->where('zona',$sitio->zona)->get();
        $transportes = DB::table('transportes')->get();
        return view('descripcion',['sitio'=>$sitio,'listahospedajes'=>$hospedajes,'listatransportes'=>$transportes]); 
    }
    public function getSitios()
    {
        $sitios = DB::table('sitios')->get();
        return view('sitios',['listaSitios'=>$sitios]);
    }
    public function getHospedajes()
    {
        $hospedajes = DB::table('hospedajes')->get(); 
        return view('hospedajes',['listahospedajes'=>$hospedajes]); 
    }
    public function getTransportes()
    {
        $transportes = DB::table('transportes')->get();
        return view('transportes',['listatransportes'=>$transportes]); 
    }
    public function getRegistro()
    {
        return view('auth.register');
    }

    public function deleteSitio(Request $request)
    {
        $sitio = Sitio::findOrFail($request->id);
        $sitio->delete();
        return redirect('/admSitio');
    }
    public function deleteHospedaje(Request $request)
    {
        $hosp = Hospedaje::findOrFail($request->id);
        Storage::delete(public_path('/image/').$hosp->galeriaS);
        $hosp->delete();
        return redirect('/admHosp');
    }
    public function deleteTransporte(Request $request)
    {
        $tran = Transporte::findOrFail($request->id);
        Storage::delete(public_path().$tran->galeriaS);
        $tran->delete();
        return redirect('/admTra');
    }

    public function postCreateSitio(Request $request)
    {

        $newsitio = new Sitio;
        $newsitio->nombre = $request->input('nombre');
        $newsitio->descripcion = $request->input('descripcion');
        $newsitio->precio = $request->input('precio');
        $file = $request->file('galeriaS');
        $file->move(public_path('/image'), $file->getClientOriginalName());
        $newsitio->galeriaS = '/image/'.$file->getClientOriginalName();
        $newsitio->zona = $request->input('zona');
        $newsitio->save();
        return redirect('/admSitio');

    }
    public function postCreateHospedaje(Request $request)
    {
        $newhosp = new Hospedaje;
        $newhosp->nombre = $request->input('nombre');
        $newhosp->nHabitaciones = $request->input('nHabitaciones');
        $file = $request->file('galeriaS');
        $file->move(public_path('/image'), $file->getClientOriginalName());
        $newhosp->galeriaS = '/image/'.$file->getClientOriginalName();
        $newhosp->precio = $request->input('precio');
        $newhosp->telefono = $request->input('telefono');
        $newhosp->direccion = $request->input('direccion');
        $newhosp->zona = $request->input('zona');
        $newhosp->save();
        return redirect('/admHosp');

    }
    public function postCreateTransporte(Request $request)
    {
        $newtran = new Transporte;
        $newtran->tipo = $request->input('tipo');
        $newtran->placas = $request->input('placas');
        $newtran->precio = $request->input('precio');
        $newtran->save();
        return redirect('/admTra');
    }
    public function putagregarReserva(Request $request)
    {
        $reserva = new Reserva;
        $reserva->id_usuario = $request->input('id_usuario');
        $reserva->id_sitio = $request->input('id_sitio');
        $reserva->id_transporte = $request->input('id_transporte');
        $reserva->id_hospedaje = $request->input('id_hospedaje');
        $reserva->fecha_ini = $request->input('fecha_ini');
        $reserva->fecha_fin = $request->input('fecha_fin');
        $reserva->save();
        return redirect('/descripcion/'.$request->id_sitio);
    }
    public function putEditHospedaje(Request $request,$id )
    {

        $file = $request->file('galeriaS');
        $name = time().$file->getClientOriginalName();
        $file->move(public_path().'/image/', $name);
        $newhosp = Hospedaje::findOrFail($id);
        $newhosp->nombre = $request->nombre;
        $newhosp->nHabitaciones = $request->nHabitaciones;
        $newhosp->galeriaS = $name;
        $newhosp->precio = $request->precio;
        $newhosp->telefono = $request->telefono;
        $newhosp->direccion = $request->direccion;
        $newhosp->zona = $request->zona;
        $newhosp->save();
        return redirect('/admHosp');

    }
    public function getEditHospedaje($id){
        $hosp = Hospedaje::findOrFail($id);
        return view('edithospedaje',compact('hosp'));
    }
 
}
