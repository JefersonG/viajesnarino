<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservas', function (Blueprint $table) {
            $table->engine='InnoDB';
            $table->increments('id');
            //-----------------------------------------
            $table->integer('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id')->on('users');
            //-----------------------------------------
            $table->integer('id_sitio')->unsigned();
            $table->foreign('id_sitio')->references('id')->on('sitios');
            //-----------------------------------------
            $table->integer('id_transporte')->unsigned();
            $table->foreign('id_transporte')->references('id')->on('transportes');
            //-----------------------------------------
            $table->integer('id_hospedaje')->unsigned();
            $table->foreign('id_hospedaje')->references('id')->on('hospedajes');
            //-----------------------------------------
            $table->date('fecha_ini');
            $table->date('fecha_fin');
            $table->timestamps();            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservas');
    }
}
