<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Hospedaje;
use App\Reserva;
use App\Sitio;
use App\Transporte;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    
    

    private function seedUsers()
    {
        DB::table('users')->delete();
        $user = new User;
        $user->cc = "1087425177";
        $user->nombre = "admin";
        $user->celular = "3155555555";
        $user->email = "admin@gmail.com.com";
        $user->rol = "ADMIN";
        $user->password = bcrypt("123");
        $user->save();

        $userb = new User;
        $userb->cc = "108745231";
        $userb->nombre = "Luis Estupiñan";
        $userb->celular = "3154534432";
        $userb->email = "estradal@gmail.com";
        $userb->password = bcrypt("123");
        $userb->save();
    }

    private function seedSitio()
    {
    }

    private function seedHospedaje()
    {
    }

    private function seedTransporte()
    {
        DB::table('transportes')->delete();
        $trans = new Transporte;
        $trans->tipo = "aerovan";
        $trans->placas = "145WAS";
        $trans->precio = 20;
        $trans->save();
    }

    public function run()
    {
        
        self::seedUsers();
        $this->command->info('Tabla Users inicilizada con datos');
        self::seedTransporte();
        $this->command->info('Tabla Transporte inicilizada con datos');
        self::seedHospedaje();
        $this->command->info('Tabla Hospedaje inicilizada con datos');
        self::seedSitio();
        $this->command->info('Tabla Sitio inicilizada con datos');
        // $this->call(UserSeeder::class);
    }
}
