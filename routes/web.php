<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ViajesController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', [ViajesController::class, 'getIndex']); 
Route::get('/home', [ViajesController::class, 'getIndex']);
Route::get('/registro', [ViajesController::class, 'getRegistro']);
Route::get('/descripcion/{id}', [ViajesController::class, 'getDescripcion']);

Route::group(['middleware'=>'auth'], function(){
    Route::get('/reservas', [ViajesController::class, 'getReservas']);    
    Route::get('/admSitio', [ViajesController::class, 'getSitios']);
    Route::get('/admHosp', [ViajesController::class, 'getHospedajes']);
    Route::get('/admTra', [ViajesController::class, 'getTransportes']);

    Route::put('admSitio',[ViajesController::class,'deleteSitio']);
    Route::post('admSitio',[ViajesController::class,'postCreateSitio']);

    Route::put('admHosp',[ViajesController::class,'deleteHospedaje']);
    Route::post('admHosp',[ViajesController::class,'postCreateHospedaje']);

    Route::put('admTra',[ViajesController::class,'deleteTransporte']);
    Route::post('admTra',[ViajesController::class,'postCreateTransporte']);

    Route::post('descripcion/{id}', [ViajesController::class, 'putagregarReserva']);
    
    Route::get('edithospedaje/{id}',[ViajesController::class,'getEditHospedaje']);
    Route::put('edithospedaje/{id}',[ViajesController::class,'putEditHospedaje']);

});



//Route::get('/home', 'HomeController@index')->name('home');






